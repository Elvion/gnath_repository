from flask import Flask
from flask import render_template
app = Flask(__name__)


import socket

def get_ip():
    host_name = socket.gethostname()
    ip_address = socket.gethostbyname(host_name)
    return ip_address 
	
@app.route("/test")
def hello():
    return "<h1 style='color:#6F4242'>Szrocik</h1>"

@app.route("/")
def hello2():
    return render_template('index.html')

@app.route("/funckje_sieciowe")
def funkcje_sieciowe():
    my_ip = get_ip()
    remote_host = 'www.amw.gdynia.pl'
    ip_of_remote_host = socket.gethostbyname(remote_host)
    return render_template('funkcje_sieciowe.html', my_ip=my_ip,remote_host=remote_host,  ip_of_remote_host=ip_of_remote_host)
	
@app.route("/pomoc")
def pomoc():
    return render_template('pomoc.html')

@app.route("/o_mnie")
def o_mnie():
    return render_template('o_mnie.html')

if __name__ == "__main__":
    app.run()
	
	

    